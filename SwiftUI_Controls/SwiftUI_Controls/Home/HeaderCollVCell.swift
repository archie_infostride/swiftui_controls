//
//  HeaderCollVCell.swift
//  SwiftUI_Controls
//
//  Created by MAC on 23/07/21.
//

import SwiftUI

struct HeaderCollVCell: View {
    
    var image : String
    var body: some View {
       Image(image)
        .resizable()
        .aspectRatio(contentMode: /*@START_MENU_TOKEN@*/.fill/*@END_MENU_TOKEN@*/)
        .frame(width: (UIScreen.main.bounds.width * 380) / 414, height: (UIScreen.main.bounds.width * 210) / 414)
        .cornerRadius(10)
    }
}

struct HeaderCollVCell_Previews: PreviewProvider {
    static var previews: some View {
        HeaderCollVCell(image: "")
            .previewLayout(.fixed(width: (UIScreen.main.bounds.width * 384) / 414, height: (UIScreen.main.bounds.width * 210) / 414))
    }
}
